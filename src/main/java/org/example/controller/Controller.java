package org.example.controller;

import org.example.domain.Passenger;
import org.example.domain.Station;
import org.example.domain.Ticket;
import org.example.domain.Train;
import org.example.es.PassengerEntity;
import org.example.es.StationEntity;
import org.example.es.TicketEntity;
import org.example.es.TrainEntity;
import org.example.service.TrainTicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Controller {

    @Autowired
    private TrainTicketService trainTicketService;


    @PutMapping("/addPassenger")
    public PassengerEntity addPassenger(
            @RequestHeader          String passengerID,
            @RequestParam("name")   String name,
            @RequestParam("gender") String gender,
            @RequestParam("age")    int age) {
        return trainTicketService.addPassenger(passengerID, name, gender, age);
    }

    @PutMapping("/addStation")
    public StationEntity addStation(
            @RequestHeader                String setStationID,
            @RequestParam("stationName")  String stationName,
            @RequestParam("train")        String train,
            @RequestParam("time")         String time) {
        return trainTicketService.addStation(setStationID, stationName, train, time);
    }


    @PutMapping("/addTrain")
    public TrainEntity addTrain(
            @RequestHeader                String train,
            @RequestParam("trainType")    String trainType,
            @RequestParam("allSeats")     int allSeats,
            @RequestParam("remainSeats")  int remainSeats) {
        return trainTicketService.addTrain(train, trainType, allSeats, remainSeats);
    }

    @PostMapping("/createTicket")
    public TicketEntity CreateTicket(
            @RequestParam("passengerID")   String passengerID,
            @RequestParam("origin")        String origin,
            @RequestParam("destination")   String destination,
            @RequestParam("train")         String train,
            @RequestParam("seatNo")        String seatNo,
            @RequestParam("classType")     String classType,
            @RequestParam("coachType")     String coachType){
        return trainTicketService.CreateTicket(passengerID, origin, destination, train, seatNo, classType, coachType);
    }


    @GetMapping("/ping")
    public String ping() {
        return "OK";
    }

    @GetMapping("/getAllPassenger")
    public List<PassengerEntity> getAllPassenger() {
        return trainTicketService.getAllPassenger(); }

    @GetMapping("/getPassengerByName")
    public PassengerEntity getPassengerByName(@RequestParam("name") String name) {
        return trainTicketService.getPassengerByName(name);
    }

    @GetMapping("/getAllStation")
    public List<StationEntity> getAllStation() {
        return trainTicketService.getAllStation();
    }

    @GetMapping("/getStationByTrain")
    public List<StationEntity> getStationByTrain(@RequestParam("train")String train){
        return  trainTicketService.getStationByTrain(train);
    }

    @GetMapping("/getAllTicket")
    public List<TicketEntity> getAllTicket() {
        return trainTicketService.getAllTicket();
    }

    @GetMapping("/getAllTrain")
    public List<TrainEntity> getAllTrain() {
        return trainTicketService.getAllTrain();
    }

    @GetMapping("/getTrainByTrainType")
    public List<TrainEntity> getTrainByTrainType(@RequestParam("trainType")String trainType) {
        return trainTicketService.getTrainByTrainType(trainType);
    }

    @DeleteMapping("/deletePassenger")
    public void deletePassenger(@RequestHeader("passengerID") String passengerID) {
        trainTicketService.deletePassenger(passengerID);
    }

    @DeleteMapping("/deleteStation")
    public void deleteStation(@RequestHeader("stationID") String setStationID){
        trainTicketService.deleteStation(setStationID);
    }

    @DeleteMapping("/deleteTicket")
    public void deleteTicket(@RequestHeader("ticketID") String ticketID){
        trainTicketService.deleteTicket(ticketID);
    }

    @DeleteMapping("/deleteTrain")
    public void deleteTrain(@RequestHeader("train") String train) {
        trainTicketService.deleteTrain(train);
    }

}
