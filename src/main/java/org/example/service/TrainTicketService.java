package org.example.service;

import org.example.es.*;
import org.example.repo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TrainTicketService {

    @Autowired
    private PassengerRepo passengerRepo;
    @Autowired
    private StationRepo   stationRepo;
    @Autowired
    private TicketRepo    ticketRepo;
    @Autowired
    private TrainRepo     trainRepo;

    @Autowired
    private void init() {
    passengerRepo.deleteAll();
    List<PassengerEntity> passengerEntities = new ArrayList<>();

    PassengerEntity passengerEntity = new PassengerEntity();
    passengerEntity.setPassengerID("001");
    passengerEntity.setName("Ni");
    passengerEntity.setGender("Male");
    passengerEntity.setAge(20);
    passengerEntities.add(passengerEntity);

    passengerEntity = new PassengerEntity();
    passengerEntity.setPassengerID("002");
    passengerEntity.setName("Nim");
    passengerEntity.setGender("Male");
    passengerEntity.setAge(9);
    passengerEntities.add(passengerEntity);
    passengerRepo.saveAll(passengerEntities);

    stationRepo.deleteAll();
    List<StationEntity> stationEntities = new ArrayList<>();

    StationEntity stationEntity = new StationEntity();
    stationEntity.setStationID("901");
    stationEntity.setStationName("Bangkok");
    stationEntity.setTrain("9");
    stationEntity.setTime("8.30");
    stationEntities.add(stationEntity);

    stationEntity = new StationEntity();
    stationEntity.setStationID("902");
    stationEntity.setStationName("Samsen");
    stationEntity.setTrain("9");
    stationEntity.setTime("8:42");
    stationEntity.setPrice(706);
    stationEntities.add(stationEntity);

    stationEntity = new StationEntity();
    stationEntity.setStationID("903");
    stationEntity.setStationName("Bang Sue Junction ");
    stationEntity.setTrain("9");
    stationEntity.setTime("8:50");
    stationEntity.setPrice(706);
    stationEntities.add(stationEntity);

    stationEntity = new StationEntity();
    stationEntity.setStationID("904");
    stationEntity.setStationName("Bangkhen");
    stationEntity.setTrain("9");
    stationEntity.setTime("9:00");
    stationEntity.setPrice(706);
    stationEntities.add(stationEntity);

    stationEntity = new StationEntity();
    stationEntity.setStationID("905");
    stationEntity.setStationName("Lak Si");
    stationEntity.setTrain("9");
    stationEntity.setTime("9:00");
    stationEntity.setPrice(709);
    stationEntities.add(stationEntity);

    stationEntity = new StationEntity();
    stationEntity.setStationID("906");
    stationEntity.setStationName("Don Mueang");
    stationEntity.setTrain("9");
    stationEntity.setTime("9:14");
    stationEntity.setPrice(711);
    stationEntities.add(stationEntity);


    stationEntity = new StationEntity();
    stationEntity.setStationID("907");
    stationEntity.setStationName("Rangsit");
    stationEntity.setTrain("9");
    stationEntity.setTime("9:21");
    stationEntity.setPrice(715);
    stationEntities.add(stationEntity);


    stationEntity = new StationEntity();
    stationEntity.setStationID("908");
    stationEntity.setStationName("Ayutthaya");
    stationEntity.setTrain("9");
    stationEntity.setTime("9:43");
    stationEntity.setPrice(735);
    stationEntities.add(stationEntity);


    stationEntity = new StationEntity();
    stationEntity.setStationID("909");
    stationEntity.setStationName("Lop Buri");
    stationEntity.setTrain("9");
    stationEntity.setTime("10:29");
    stationEntities.add(stationEntity);
    stationEntity.setPrice(764);

    stationEntity = new StationEntity();
    stationEntity.setStationID("910");
    stationEntity.setStationName("Nakhon Sawan");
    stationEntity.setTrain("9");
    stationEntity.setTime("11:38");
    stationEntity.setPrice(810);
    stationEntities.add(stationEntity);

    stationEntity = new StationEntity();
    stationEntity.setStationID("911");
    stationEntity.setStationName("Phitsanulok");
    stationEntity.setTrain("9");
    stationEntity.setTime("13:19");
    stationEntity.setPrice(889);
    stationEntities.add(stationEntity);

    stationEntity = new StationEntity();
    stationEntity.setStationID("912");
    stationEntity.setStationName("Sila At");
    stationEntity.setTrain("9");
    stationEntity.setTime("14:52");
    stationEntity.setPrice(922);
    stationEntities.add(stationEntity);

    stationEntity = new StationEntity();
    stationEntity.setStationID("913");
    stationEntity.setStationName("Denchai");
    stationEntity.setTrain("9");
    stationEntity.setTime("15:53");
    stationEntities.add(stationEntity);
    stationEntity.setPrice(967);

    stationEntity = new StationEntity();
    stationEntity.setStationID("914");
    stationEntity.setStationName("Nakhon Lampang");
    stationEntity.setTrain("9");
    stationEntity.setTime("18:11");
    stationEntity.setPrice(1004);
    stationEntities.add(stationEntity);


    stationEntity = new StationEntity();
    stationEntity.setStationID("915");
    stationEntity.setStationName("Khun Tan");
    stationEntity.setTrain("9");
    stationEntity.setTime("18:11");
    stationEntity.setPrice(1018);
    stationEntities.add(stationEntity);

    stationEntity = new StationEntity();
    stationEntity.setStationID("916");
    stationEntity.setStationName("Lamphun");
    stationEntity.setTrain("9");
    stationEntity.setTime("20:05");
    stationEntity.setPrice(1033);
    stationEntities.add(stationEntity);


    stationEntity = new StationEntity();
    stationEntity.setStationID("917");
    stationEntity.setStationName("Chiang Mai");
    stationEntity.setTrain("9");
    stationEntity.setTime("20:30");
    stationEntity.setPrice(1041);
    stationEntities.add(stationEntity);

    stationRepo.saveAll(stationEntities);


    trainRepo.deleteAll();
    List<TrainEntity> trainEntities = new ArrayList<>();
    TrainEntity trainEntity = new TrainEntity();
    trainEntity.setTrain("9");
    trainEntity.setTrainType("SP EXP");
    trainEntity.setAllSeats(396);
    trainEntity.setRemainSeats(396);
    trainEntities.add(trainEntity);

    trainEntity = new TrainEntity();
    trainEntity.setTrain("10");
    trainEntity.setTrainType("SP EXP");
    trainEntity.setAllSeats(396);
    trainEntity.setRemainSeats(396);
    trainEntities.add(trainEntity);
    trainRepo.saveAll(trainEntities);

    }

    public List<PassengerEntity> getAllPassenger() {
        return passengerRepo.findAll();
    }
    public PassengerEntity getPassengerByName(String name) {
        return passengerRepo.findByName(name);
    }
   public PassengerEntity getPassengerByPassengerID(String passengerID){return passengerRepo.findByPassengerID(passengerID);}
    public PassengerEntity addPassenger(String passengerID, String name, String gender, int age){
        PassengerEntity passengerEntity = new PassengerEntity();
        passengerEntity.setPassengerID(passengerID);
        passengerEntity.setName(name);
        passengerEntity.setGender(gender);
        passengerEntity.setAge(age);
        return passengerRepo.save(passengerEntity);
    }
    public void deletePassenger(String passengerID) {passengerRepo.deleteById(passengerID);}

    public List<StationEntity> getAllStation() { return  stationRepo.findAll(); }
    public StationEntity getInfoStation(String stationName){ return stationRepo.findByStationName(stationName); }

    public List<StationEntity> getStationByTrain(String train) { return stationRepo.findByTrain(train);}

    public StationEntity addStation(String stationID, String stationName, String train, String time){
        StationEntity stationEntity = new StationEntity();
        stationEntity.setStationID(stationID);
        stationEntity.setStationName(stationName);
        stationEntity.setTrain(train);
        stationEntity.setTime(time);
        return stationRepo.save(stationEntity);
    }
    public void deleteStation(String setStationID) {
        stationRepo.deleteById(setStationID);
    }

    public List<TicketEntity> getAllTicket() {
        return  ticketRepo.findAll();
    }
    public TicketEntity CreateTicket(String passengerID, String origin, String destination, String train, String seatNo, String classType, String coachType) {
        TrainEntity t = getTrainByTrain(train);
        if(t.getRemainSeats() >= 1)
        {
            t.setRemainSeats(t.getRemainSeats() - 1);
            trainRepo.save(t);
        }

        StationEntity getInfoO =  getInfoStation(origin);
        StationEntity getInfoD =  getInfoStation(destination);
        PassengerEntity getInfoP = getPassengerByPassengerID(passengerID);


        int Price = 0 ;
        int pDiscount = getInfoD.getPrice();
        if(getInfoP.getAge()<10 || getInfoP.getAge()>60){
            Price = (pDiscount * 90)/100;
        }else{
            Price = pDiscount;
        }

        TicketEntity ticketEntity = new TicketEntity();
        ticketEntity.setPassengerID(passengerID);
        ticketEntity.setPassengerName(getInfoP.getName());
        ticketEntity.setOrigin(origin);
        ticketEntity.setDestination(destination);
        ticketEntity.setTrain(train);
        ticketEntity.setDepartureDate(LocalDate.now());
        ticketEntity.setDepTime(getInfoO.getTime());
        ticketEntity.setArrTime(getInfoD.getTime());
        ticketEntity.setSeatNo(seatNo);
        ticketEntity.setClassType(classType);
        ticketEntity.setCoachType(coachType);
        ticketEntity.setPrice(Price);

        return ticketRepo.save(ticketEntity);
    }
    public void deleteTicket(String ticketID) {
        ticketRepo.deleteById(ticketID);
    }

    public List<TrainEntity> getAllTrain() {return trainRepo.findAll(); }
    public List<TrainEntity> getTrainByTrainType(String trainType) {return trainRepo.findByTrainType(trainType); }
    public TrainEntity getTrainByTrain(String train) {
        return trainRepo.findByTrain(train);
    }
    public TrainEntity addTrain(String train, String trainType, int allSeats, int remainSeats){
        TrainEntity trainEntity = new TrainEntity();
        trainEntity.setTrain(train);
        trainEntity.setTrainType(trainType);
        trainEntity.setAllSeats(allSeats);
        trainEntity.setRemainSeats(remainSeats);

        return trainRepo.save(trainEntity);
    }
    public void deleteTrain(String train) {
        trainRepo.deleteById(train);
    }


}
