package org.example.repo;

import org.example.es.PassengerEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface PassengerRepo extends ElasticsearchRepository<PassengerEntity, String> {
    List<PassengerEntity> findAll();
    PassengerEntity findByPassengerID(String passengerID);
    PassengerEntity findByName(String name);
}
