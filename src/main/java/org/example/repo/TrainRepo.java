package org.example.repo;

import org.example.es.TrainEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface TrainRepo extends ElasticsearchRepository<TrainEntity, String>{
    List<TrainEntity> findAll();
    List<TrainEntity> findByTrainType(String trainType);
    TrainEntity findByTrain(String train);

}
