package org.example.repo;

import org.example.es.StationEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface StationRepo extends ElasticsearchRepository<StationEntity, String>{
    List<StationEntity> findAll();
    List<StationEntity> findByTrain(String train);
    StationEntity findByStationName(String stationName);
}
