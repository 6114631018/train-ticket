package org.example.repo;

import org.example.es.TicketEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface TicketRepo extends ElasticsearchRepository<TicketEntity, String>{
    List<TicketEntity> findAll();
}
